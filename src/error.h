#ifndef ERROR_H 
#define ERROR_H 

#include <stdlib.h>
#include <stdio.h>

void die(const char* s); 

#endif
