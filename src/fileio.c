#include "fileio.h"

void read_curr_dir() {
    int count = 0;
    while(readdir(cfg.curr_dir) != NULL) count++;
    rewinddir(cfg.curr_dir);
    
    cfg.content_len = count - 2;
    cfg.content = realloc(cfg.content, sizeof(struct dirent) * cfg.content_len);
    
    struct dirent* tmp;
    int idx = 0;

    while((tmp = readdir(cfg.curr_dir)) != NULL) {
        if(strcmp(tmp->d_name, "..") == 0 || strcmp(tmp->d_name, ".") == 0) continue;
        
        // TODO find better way for sorting by directories first XD
        if(tmp->d_type == 4) {
            memmove(&cfg.content[1], &cfg.content[0], sizeof(struct dirent) * idx);
            cfg.content[0] = *tmp;
        } else {
            cfg.content[idx] = *tmp; 
        }

        idx++;
    }
    
    rewinddir(cfg.curr_dir);
    free(tmp);
}

void clear_selected() {
    while(cfg.selected_len) {
        cfg.selected[cfg.selected_len--] = -1;
    }
}

void open_dir(const char* dir) {
    if(cfg.curr_dir != NULL) closedir(cfg.curr_dir);
    if((cfg.curr_dir = opendir(dir)) == NULL) die("open dir");
    if(chdir(dir) != 0) die("Changing directory");

    clear_selected();

    cfg.curr_row = 0;
    cfg.offset = 0;
    
    char buf[MAX_PATH_LEN];
    getcwd(buf, MAX_PATH_LEN);
    
    cfg.path_len = strlen(buf);
    cfg.curr_path = realloc(cfg.curr_path, cfg.path_len);
    memcpy(cfg.curr_path, buf, cfg.path_len);

    read_curr_dir();
}

void create_file() {
    char* filename = prompt("Enter filename:");

    if(filename != NULL) {
        if(access(filename, F_OK) == 0) {
            status_message("File already exists!");
        } else {
            if(creat(filename, 0664) == -1) status_message("Creating file failed!");
            status_message("Here we go!");
            read_curr_dir();
        }

        free(filename);
    }
}

void create_dir() {
    char* dirname = prompt("Enter directory name:");
    if(dirname != NULL) {
        if(mkdir(dirname, 0700) == -1) { 
            status_message("Failed creating directory! Directory may exist.");
        } else {
            status_message("Created new directory!");
            read_curr_dir();
        }
        free(dirname);
    }
}

void delete_entry() {
    if(cfg.content_len == 0) {
        status_message("Nothing to delete!");
        return;
    }

    int buff_len = strlen(cfg.content[cfg.curr_row].d_name) + 16;
    char buff[buff_len];

    if(cfg.selected_len == 0) {
        snprintf(buff, buff_len, "Delete %s? (y/n):", cfg.content[cfg.curr_row].d_name);
    }

    char* choice = prompt(cfg.selected_len == 0 ? buff : "Delete selected items? (y/n):");

    if(choice != NULL) {
        if(strcmp(choice, "y") == 0) {
            if(cfg.selected_len != 0) {
                char* error_msg = "Couldn't delete these entries: ";
                int was_error = 0;

                for(int i = 0; i < cfg.selected_len; i++) {
                    if(remove(cfg.content[cfg.selected[i]].d_name) == -1) { 
                        was_error = 1;
                        strcat(error_msg, cfg.content[cfg.selected[i]].d_name);
                        strcat(error_msg, " ");
                    }
                }

                if(was_error)
                    status_message(error_msg);
                else
                    status_message("Success!");

                read_curr_dir();
                cfg.curr_row = 0;
                
                clear_selected();
            } else {
                if(remove(cfg.content[cfg.curr_row].d_name) == -1) { 
                    status_message("Couldn't delete given entry!");
                } else {
                    status_message("Success!");
                    read_curr_dir();
                    cfg.curr_row--;
                }
            }
        } else if(strcmp(choice, "n") == 0) {
            status_message("Aborted!");
        }

        free(choice);
    }
}

void select_entry() {
    //Check if entry is already selected
    for(int i = 0; i < cfg.selected_len; i++) {
        if(cfg.selected[i] == cfg.curr_row) {
            memmove(&cfg.selected[i], &cfg.selected[i + 1], sizeof(int) * cfg.selected_len - i + 1);
            cfg.selected_len--;
            return;
        }
    }

    if(cfg.selected_len == cfg.selected_capacity) {
        cfg.selected_capacity += 3;
        cfg.selected = realloc(cfg.selected, cfg.selected_capacity);
    }

    cfg.selected[cfg.selected_len++] = cfg.curr_row;
}

void save_entry(char* name, FILE* file) {
    cfg.copied_names = realloc(cfg.copied_names, sizeof(char*) * (cfg.copied_capacity + 1));
    int name_len = strlen(name);
    cfg.copied_names[cfg.copied_len] = malloc(name_len + 1);
    memcpy(cfg.copied_names[cfg.copied_len], name, name_len + 1);

    cfg.copied_files = realloc(cfg.copied_files, sizeof(FILE*) * (cfg.copied_capacity + 1)); 
    cfg.copied_files[cfg.copied_len] = file;
    
    cfg.copied_capacity++;
    cfg.copied_len++;
}

void free_copied_entries() {
    for(int i = 0; i < cfg.copied_len; i++) {
        fclose(cfg.copied_files[i]);
        free(cfg.copied_names[i]);
    }
    
    char** names = cfg.copied_names;
    FILE**  files = cfg.copied_files;

    free(names);
    free(files);

    cfg.copied_names = NULL;
    cfg.copied_files = NULL;

    cfg.copied_len = 0;
    cfg.copied_capacity = 0;
}

void copy_entry() {
    if(cfg.selected_len == 0) {
        if(cfg.content[cfg.curr_row].d_type == 4) {
            status_message("You can copy files only");
            return;
        }

        FILE *src;

        src = fopen(cfg.content[cfg.curr_row].d_name, "r");

        if(src == NULL) { 
            fclose(src);
            status_message("Failed to copy file!");
            return;
        }

        save_entry(cfg.content[cfg.curr_row].d_name, src);
        status_message("Entry copied!");
    } else {
        for(int i = 0; i < cfg.selected_len; i++) {
            if(cfg.content[cfg.selected[i]].d_type == 4) { 
                free_copied_entries();
                clear_selected();
                status_message("You can copy files only!");
                return;
            };

            FILE* src;

            src = fopen(cfg.content[cfg.selected[i]].d_name, "r");

            if(src == NULL) { 
                fclose(src);
                free_copied_entries();
                clear_selected();
                status_message("Failed to copy!");
                return;
            }

            save_entry(cfg.content[cfg.selected[i]].d_name, src);
        }

        clear_selected();
        status_message("Entries copied!");
    }
}

void paste_entry() {
    if(cfg.copied_names == NULL && cfg.copied_files == NULL) { 
        status_message("Nothing to copy!"); 
        return; 
    }
    
    char* new_name = NULL;

    if(cfg.copied_len == 1) {
        new_name = prompt("Enter filename (press enter to save with original name):");
    }
    
    for(int i = 0; i < cfg.copied_len; i++) {
        FILE* dest;

        if(new_name == NULL)
            dest = fopen(cfg.copied_names[i], "w");
        else
            dest = fopen(new_name, "w");

        if(dest == NULL) {
            fclose(dest);
            free_copied_entries();
            die("COPYING");
            status_message("Failed to copy file!");
            return;
        }

        char c;
        while((c = fgetc(cfg.copied_files[i])) != EOF) {
            fputc(c, dest);
        }
        
        fclose(dest);
    }

    if(new_name != NULL)
        free(new_name);

    free_copied_entries();
    status_message("Success!");
    read_curr_dir();
}
