#ifndef INPUT_H
#define INPUT_H

#include <unistd.h>
#include <error.h>
#include <errno.h>
#include <stdlib.h>
#include "config.h"
#include "fileio.h"

int read_key(); 
void process_keypress(); 

#endif
