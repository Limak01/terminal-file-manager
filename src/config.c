#include "config.h"

struct config cfg;

int get_terminal_size(int* rows, int* cols) {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    *rows = w.ws_row;
    *cols = w.ws_col;

    return 1;
}

void init_cfg() {
    cfg.curr_dir = NULL;
    cfg.offset = 0;
    cfg.curr_row = 0;
    cfg.row_size = 50;
    cfg.curr_path = NULL;
    cfg.path_len = 0;
    cfg.content = NULL;
    cfg.content_len = 0;
    cfg.status_msg[0] = '\0';
    cfg.status_msg_time = 0;
    cfg.selected_len = 0;
    cfg.selected_capacity = 3;
    cfg.selected = malloc(sizeof(int) * cfg.selected_capacity);
    cfg.copied_len = 0;
    cfg.copied_capacity = 0;
    cfg.copied_names = NULL;
    cfg.copied_files = NULL;

    if(get_terminal_size(&cfg.rows, &cfg.cols) == -1) die("terminal size");
    cfg.rows -= 2;
}
