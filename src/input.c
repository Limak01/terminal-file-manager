#include "input.h"

int read_key() {
    int nread;
    char c;

    while((nread = read(STDIN_FILENO, &c, 1)) != 1) {
        if(nread == -1 && errno != EAGAIN) die("Read");
    } 

    return c;
}

void process_keypress() {
    int c = read_key();

    switch(c) {
        case 'q':
            exit(0);
        break;
        case DOWN:
            if(cfg.curr_row - cfg.offset < cfg.rows - 1 && cfg.curr_row < cfg.content_len - 1)
                cfg.curr_row++;
        break;
        case UP:
            if(cfg.curr_row > 0)
                cfg.curr_row--;
        break;
        case PREV:
            open_dir("..");
        break;
        case NEXT:
            if(cfg.content[cfg.curr_row].d_type == 4)
                open_dir(cfg.content[cfg.curr_row].d_name);
        break;
        case SELECT:
            select_entry();
        break;
        case CREATE_FILE:
            create_file();
        break;
        case DELETE_ENTRY:
            delete_entry();
        break;
        case CREATE_DIR:
            create_dir();
        break;
        case COPY_ENTRY:
            copy_entry();
        break;
        case PASTE_ENTRY:
            paste_entry();
        break;
    }
}

