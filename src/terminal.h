#ifndef TERM_H
#define TERM_H

#include <termio.h>
#include <stdlib.h>
#include <unistd.h>
#include "error.h"

extern struct termios org_term;
void disable_raw_mode();
void enable_raw_mode();

#endif

