#ifndef FILEIO_H
#define FILEIO_H

#include "config.h"
#include "error.h"
#include "output.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <fcntl.h>
#include <sys/stat.h>

#define MAX_PATH_LEN 100

void open_dir(const char* dir);
void create_file();
void delete_entry();
void create_dir();
void select_entry();
void clear_selected();
void save_entry(char* name, FILE* file);
void free_copied_entries();
void copy_entry();
void paste_entry();

#endif
