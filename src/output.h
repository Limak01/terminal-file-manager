#ifndef OUTPUT_H
#define OUTPUT_H

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include "config.h"
#include "input.h"

#define INIT_BUFF {0, NULL}

struct buff {
    int len;
    char* b;
};

void status_message(const char* msg);
char* prompt(char* p);
void draw_message_bar(struct buff* bf); 
void draw_status_bar(struct buff* bf);
void draw();
void scroll();
void refresh_window();


#endif
