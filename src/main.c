#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include "config.h"
#include "terminal.h"
#include "input.h"
#include "output.h"
#include "fileio.h"

int main(int argc, char** argv) {
    enable_raw_mode();
    init_cfg();

    if(argc == 1) {
        open_dir(".");
    } else {
        open_dir(argv[1]);
    }
    
    while(1) {
        refresh_window();
        process_keypress();
    }

    return 0;
}
