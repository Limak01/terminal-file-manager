#ifndef CONFIG_H
#define CONFIG_H

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <time.h>
#include "error.h"

#define CTR_KEY(k) ((k) & 0x1f)

enum KEYS {
    BACKSPACE = 127,
    UP = 'k',
    DOWN = 'j',
    NEXT = 'l',
    PREV = 'h',
    SELECT = 's',
    ESC_KEY = '\x1b',
    CREATE_FILE = 'f',
    CREATE_DIR = 'd',
    DELETE_ENTRY = 'p',
    COPY_ENTRY = 'c',
    PASTE_ENTRY = 'v',
};

struct config {
    int rows, cols;
    int offset;
    int curr_row;
    int row_size;
    char status_msg[100];
    time_t status_msg_time;
    char* curr_path;
    int path_len;
    DIR* curr_dir;
    struct dirent* content;
    int content_len;
    int selected_len, selected_capacity;
    int* selected;
    
    int copied_capacity;
    int copied_len;
    char** copied_names;
    FILE** copied_files;
};

extern struct config cfg;
void init_cfg();

#endif
