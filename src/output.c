#include "output.h"

static void buff_append(struct buff* bf, char* s, int len) {
    char* new = realloc(bf->b, bf->len + len );

    if(new == NULL) return;
    
    memcpy(&new[bf->len], s, len);
    bf->b = new;
    bf->len += len;
}

static void buff_free(struct buff* bf) {
    free(bf->b);
}


void status_message(const char* msg) {
    int msg_len = strlen(msg) + 1;

    if(msg_len > (int)sizeof(cfg.status_msg)) msg_len = sizeof(cfg.status_msg);
    
    memcpy(cfg.status_msg, msg, msg_len);
    cfg.status_msg_time = time(NULL);
}

char* prompt(char* p) {
    size_t buff_size = 100;
    char* buff = malloc(buff_size);
    char message[buff_size];
    int idx = 0;
    
    buff[0] = '\0';

    while(1) {
        snprintf(message, buff_size, "%s %s", p, buff);
        status_message(message);
        refresh_window();

        int c = read_key();

        if(c == '\r') {
            if(idx == 0) {
                status_message("Input some name idiot...");
                free(buff);
                return NULL;
            }

            status_message("");
            break;
        } else if(c == BACKSPACE) {
            if(idx != 0) buff[--idx] = 0;
        } else if(c == ESC_KEY) {
            status_message("Aborted!");
            free(buff);
            return NULL;
        } else {
            if(isprint(c)) {
                buff[idx++] = c;
                buff[idx] = '\0';
            }
        }

    }

   return buff; 
}

void draw_message_bar(struct buff* bf) {
    int msg_len = strlen(cfg.status_msg);
     
    if(msg_len > cfg.cols) msg_len = cfg.cols;

    if(msg_len && time(NULL) - cfg.status_msg_time < 5) {
        buff_append(bf, cfg.status_msg, msg_len);
    }
}

void draw_status_bar(struct buff* bf) {
    buff_append(bf, "\x1b[34m", 5);
    buff_append(bf, cfg.curr_path, cfg.path_len);
    buff_append(bf, "\x1b[m", 4);
    buff_append(bf, "\r\n", 2);
}

void draw(struct buff* bf) {
    char buf[cfg.row_size];
     
    for(int i = 0; i < cfg.rows; i++) {
        int curr = i + cfg.offset;
        if(curr < cfg.content_len) {
            int len = strlen(cfg.content[curr].d_name);
            if(len > cfg.row_size) len = cfg.row_size;

            for(int j = 0; j < cfg.row_size; j++) {
                buf[j] = ' ';
            }

            memcpy(buf, cfg.content[curr].d_name, len);

            int isSelected = 0;

            for(int j = 0; j < cfg.selected_len; j++) {
                if(curr == cfg.selected[j]) {
                    isSelected = 1;
                }
            }


            if(curr == cfg.curr_row) {
                buff_append(bf, "\x1b[7m", 4);
            }


            if(cfg.content[curr].d_type == 4) {
                if(isSelected)
                    buff_append(bf, "\x1b[33m", 5);
                else
                    buff_append(bf, "\x1b[34m", 5);

                buff_append(bf, "   ", 7);
            } else {
                if(isSelected)
                    buff_append(bf, "\x1b[33m", 5);

                buff_append(bf, "   ", 6);
            }


            buff_append(bf, "\x1b[1m", 4);
            buff_append(bf, buf, cfg.row_size);
            buff_append(bf, "\x1b[m\r\n", 6);
        } else {
            buff_append(bf, "\r\n", 2);
        }
    } 
}

void scroll() {
    if(cfg.curr_row - cfg.offset + 5 > cfg.rows - 1 && cfg.curr_row + 5 < cfg.content_len) {
        cfg.offset++;
    }

    if(cfg.curr_row - cfg.offset - 5 < 0 && cfg.curr_row >= 5) {
        cfg.offset--;
    }
}

void refresh_window() {
    struct buff b = INIT_BUFF;
    
    scroll();

    buff_append(&b, "\x1b[2J", 4);
    buff_append(&b, "\x1b[H", 3);

    draw(&b);
    draw_status_bar(&b);
    draw_message_bar(&b);
    
    buff_append(&b, "\x1b[H", 3);
    //buff_append(&b, "\x1b[?25l", 6);
    write(STDOUT_FILENO, b.b, b.len);
    buff_free(&b);
}
