#include "terminal.h"

struct termios org_term;

void disable_raw_mode() {
    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &org_term) == -1) return;
}

void enable_raw_mode() {
    if(tcgetattr(STDIN_FILENO, &org_term) == -1) die("Enabling raw mode error"); 
    atexit(disable_raw_mode);

    struct termios raw_term = org_term;
    raw_term.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw_term.c_oflag &= ~(OPOST);
    raw_term.c_cflag |= (CS8);
    raw_term.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    raw_term.c_cc[VMIN] = 0;
    raw_term.c_cc[VTIME] = 1;

    if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw_term) == -1) die("Enabling raw mode error"); 
}


