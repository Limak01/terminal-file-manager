![Screen from extension](./fm.png?raw=true "fm")

If you want to use it just git clone it, enter to cloned directory and run make

# Controls

|Keys|Description|
|---|---|
|<kbd>k</kbd>, <kbd>j</kbd>|Move cursor up and down|
|<kbd>h</kbd>, <kbd>l</kbd>|Switch to previous, next directory|
|<kbd>q</kbd>|Quit|
|<kbd>s</kbd>|Select current item|
|<kbd>f</kbd>|Create new file|
|<kbd>d</kbd>|Create new directory|
|<kbd>p</kbd>|Delete current or selected items|
|<kbd>c</kbd>|Copy file to buffer|
|<kbd>v</kbd>|Paste file from buffer|
