flim: src
	mkdir -p build
	$(CC) src/*.c src/*.h -o build/flim -Wall -Wextra -pedantic -std=c99
